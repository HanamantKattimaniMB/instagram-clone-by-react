import './App.css';
import { Component } from 'react';
import Signup from './Components/Signup';
import Login from './Components/Login';
import Upload from './Components/Upload'
import Navbar from './Components/Navbar'
import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Home from './Components/Home';

import {connect} from "react-redux";
import BookmarkedPage from './Components/BookmarkedPage';

class App extends Component {
 
  render() {
    {console.log(this.props.loggedIn)}

    return (
      <div className="App">
      <Router>
        {this.props.loggedIn ?  <Navbar/>:false}
       
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/login" component={Login}/>
          <Route path="/home" component={Home}/>
          <Route path="/signup" component={Signup}/>
          <Route path="/upload" component={Upload}/>
          <Route path="/bookmark" component={BookmarkedPage}/>
        </Switch>
      </Router>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loggedIn:state.onLoginState
  }
}

export default connect(mapStateToProps)(App);
