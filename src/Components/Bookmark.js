import React, { Component } from 'react';

class Bookmark extends Component {

    constructor(props) {
        super(props);
        this.state = {
            bookmarked:props.bookmarked
        }
    }

    render() {
        return (
            <div onClick={()=>{
                this.setState({bookmarked:!this.state.bookmarked})

                if(this.state.bookmarked) {
                    this.props.addBookmark()
                }   else {
                    this.props.removeBookmark()
                }
            }}>
                { this.state.bookmarked ?

                <i className="col-2 far fa-bookmark fa-2x px-0 text-secondary"></i>
                :
                <i className="col-2 fas fa-bookmark text-primary fa-2x px-0"></i>}
            </div>
        );
    }
}

export default Bookmark;