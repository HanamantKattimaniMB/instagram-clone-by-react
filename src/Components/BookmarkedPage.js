import React, { Component } from 'react'
import { connect } from "react-redux"
import {onChangeBookmark} from "../Redux/action"

const axios = require('axios')

class BookmarkedPage extends Component {
    getBookmarkedPosts  =  async ()  =>  {
        const posts = await   axios.get(`https://instagram-clone-backend-hk.herokuapp.com/api/post`, {
            "email": localStorage.getItem("user")
        })

        const Bookmarks = await axios.post(`https://instagram-clone-backend-hk.herokuapp.com/api/bookmarkOfCurrentUser`, {
            "userID": localStorage.getItem("user_id")
        })

        console.log(posts,"------------------------------")
        
        
        this.props.onChangeBookmark(Bookmarks.data)
       
        console.log(Bookmarks,"Bookmarked post id")
    }
    componentDidMount() {
        this.getBookmarkedPosts()
    }

    componentDidUpdate () {
        console.log(this.props.arrayOfBookMarkedPosts)
    }
    
    render() {
        return (
            <div>
                <div className="col-8 mt-5">
                    <h1 className="mt-5" >Bookmarked post are here{this.props.arrayOfBookMarkedPosts}</h1>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        arrayOfBookMarkedPosts:state.arrayOfBookMarkedPosts
    }
}

const mapDispatchToProps = {
    onChangeBookmark
}
export default connect(mapStateToProps, mapDispatchToProps)(BookmarkedPage)