import React, { Component } from 'react';
import Navbar from './Navbar';
import PostContainer from './PostContainer';
import Suggestion from './Suggestion';

class Home extends Component {
    render() {
        return (
            <div>
                <Navbar/>
                <PostContainer/>
                <Suggestion/>  
            </div>
        );
    }
}

export default Home;