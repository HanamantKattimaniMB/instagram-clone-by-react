import React, { Component } from 'react';
import { connect } from "react-redux"
import { onChangeArrayOfLikedPost } from "../Redux/action"

class Like extends Component {

    toggleLikes = () => {
        const newArrayOfPost = this.props.arrayPost.filter(post => {
          return post.post_id !== this.props.post.post_id
        })

        const currentPost = this.props.arrayPost.find(post => {
            return post.post_id === this.props.post.post_id
          })

          const stateOfLiked = !currentPost["liked"]
          const updatedCurrentPost = {
                ...currentPost, 
                liked:stateOfLiked
        }
        newArrayOfPost.push(updatedCurrentPost)
        this.props.onChangeArrayOfLikedPost(newArrayOfPost)
    }
   
    render() {
        return (
            <div onClick={()=>{
                if(this.props.post.liked == true) {
                    this.props.removeLike()
                    this.toggleLikes()                          
                }   else {
                    this.props.addLike()
                    this.toggleLikes()
                }  
            }}>
                
                { !this.props.post.liked ?

                <i className="col-2 far fa-heart fa-2x px-0 text-secondary"></i>
                :
                <i className="col-2 fas fa-heart text-danger fa-2x px-0"></i>}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
     likeState:state.likeState,
     arrayPost:state.arrayPost
    }
}

const mapDispatchToProps = {
    onChangeArrayOfLikedPost
}

export default connect(mapStateToProps, mapDispatchToProps)(Like)
