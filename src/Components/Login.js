import React, { Component } from 'react';
import validator from "../Methods/joiValidation"
import {Link} from 'react-router-dom'
import * as ReactBootstrap from 'react-bootstrap'
import {connect} from "react-redux";
import { 
        onChangeIsEnable,
        onChangeSpinner,
        onChangeEnableButton,
        onChangeIsValidUser,
        setInputData,
        onLogin,
       } from "../Redux/action"

const axios = require('axios');

class Login extends Component {

    loginDataValidation = async (event) => {
        const { name, value } = event.target
        const result = await validator({ [name]: value })
        this.props.setInputData({[name]: value})

        if (result !== "success") {
            this.props.setInputData({
                error: {
                    ...this.props.error,
                    [name]: result
                }
            })
        } else {
            this.props.setInputData({
                error: {
                    [name]: ""
                }
            })
        }
    }

    userAuthentication = () => {

        this.props.onChangeSpinner("true")
        this.props.onChangeEnableButton("true")

        axios.post("https://instagram-clone-backend-hk.herokuapp.com/api/authentication", {
            email: this.props.email,
            password: this.props.password,
        })
        .then((response) => {
            console.log(response.data.isValidUser)
            console.log(response.data)

            if (response.data.isValidUser) {
                localStorage.setItem("user",this.props.email)
                localStorage.setItem("user_id",response.data.userData[0].user_id)
                localStorage.setItem("user_name",response.data.userData[0].user_name)
                localStorage.setItem("profile_picture",response.data.userData[0].profile_picture)
                localStorage.setItem("full_name",response.data.userData[0].full_name)   
                this.props.onLogin(true)                 
                this.props.history.push("/home")
            } else {
                this.props.onChangeSpinner("false")
            }
            this.props.onChangeIsValidUser(response.data.isValidUser)
            this.props.onChangeEnableButton("false")
           
        }).catch((error) => {
            console.log(error)
        })
    }

    render() {
        return (
            <div className="col-md-12 border d-flex flex-column justify-content-center align-items-center" style={{ backgroundColor: "#fafafa", height: "50rem" }}>
                <div className=" border mt-0" style={{ height: "450px", width: "350px", backgroundColor: "#ffffff" }}>
                    <h2 className="mt-5">Instagram</h2>

                    <div className="col-10 ml-4 mt-5" style={{ height: "14rem" }}>

                        <input disabled={ this.props.enableButton ==="false" ? false:true} 
                            className="form-control mt-1" name="email" type="text" 
                            onChange={this.loginDataValidation} placeholder="Email">
                            </input>

                        <span className="text-danger">{this.props.error.email}</span>

                        <input disabled={ this.props.enableButton ==="false" ? false:true} 
                            className="form-control mt-1" name="password" type="password" 
                            onChange={this.loginDataValidation} placeholder="Password">
                            </input>

                        <span className="text-danger">{this.props.error.password}</span>
                        
                        <button disabled = { this.props.enableButton === "false"

                            && this.props.email && this.props.password  !== "" 
                            && (this.props.error.password === "" || this.props.error.password === undefined) 
                            && (this.props.error.email === "" || this.props.error.email ===undefined) ? false:true }

                            className="col-md-12 btn btn-primary btn-lg mt-3" onClick={this.userAuthentication} type="button">
    
                            {this.props.spinner === "true" ? <ReactBootstrap.Spinner animation="border" variant="white"/>:""} 
                            {this.props.enableButton ==="false" ? <div> Login</div>:<div></div>}              
                            </button>
                    </div>

                    <div className="col-12 ">
                        <span className="text-danger col-md-12">{this.props.isValidUser === "true" ? "" : "The username you entered doesn't belong to an account. Please check your username and try again."}</span>
                    </div>
                </div>

                <div className="mt-3 px-0 border " style={{ width: "22rem" }}>
                    <div className="mt-3">Don't have an account? <Link to = "/signup" >sign up</Link></div>
                    <div className="mt-3">Get the app.</div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        email: state.email,            
        password: state.password,
        isEnable: state.isEnable,
        isValidUser: state.isValidUser,
        spinner: state.spinner,
        enableButton: state.enableButton,
        error:state.error,
    }
}

const mapDispatchToProps = {
    onChangeIsEnable,
    onChangeSpinner,
    onChangeEnableButton,
    onChangeIsValidUser,
    setInputData,
    onLogin,
}
export default connect(mapStateToProps, mapDispatchToProps)(Login)