import React, { Component } from 'react';
import {Link} from "react-router-dom"

class Navbar extends Component {
    constructor(props) {
        super()
    }

    displayBookmarksOnly = () => {
        this.props.history.push("/bookmark")
    }

    render() {
        return (
            <nav className="navbar  col-12 navbar-default fixed-top  navbar-expand-lg navbar-light  justify-content-md-around row bg-white">
                
                <a className=" logo col-md-4 col-4 text-center  navbar-brand row" href="example.com" style={{color: "black"}}> 
                    <div> 
                        <Link className="text-secondary" to = "/upload" > <i class="far fa-plus-square fa-2x"></i> </Link> 
                         | Instagram 
                    </div>
                 </a>
                <form className="form-inline my-2 my-lg-0 col-xs-1 col-md-4 d-none d-md-block text-center">
                    <input className="form-control form mr-sm-2 " type="search" placeholder="Search" aria-label="Search" style={{backgroundColor:"#fafafa"}}/>
                </form>

                <div className="col-md-4 d-flex col-8 row justify-content-md-around justify-content-between">
                    <i className="fas fa-home fa-2x"></i>
                    <i className="fab fa-facebook-messenger fa-2x"></i>
                    <i className="far fa-compass fa-2x"></i>
                    <i className="far fa-heart fa-2x"></i>
                    <i className="fas fa-user fa-2x"></i>
                    <Link className="text-secondary" to = "/bookmark" > <i class="far fa-bookmark fa-2x"></i> </Link> 
                </div> 
            </nav>
        );
    }
}

export default Navbar;  