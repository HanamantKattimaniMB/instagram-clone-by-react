// import React, { Component } from 'react';
// import Bookmark from './Bookmark';
// import Like from './Like';

// class PostCard extends Component {
//     render() {
//         return (
//             <div className="card col-12 pt-5 align-items-center pr-3  ml-md-5" style={{ width: "40rem" }}>
//                             <div className="card-body col-12 row justify-content-between">
//                                 <img className="col-3 col-md-2  rounded-circle " src={this.props.post_data.profile_picture} alt="Img not loaded" style={{ height: "50px", width: "50px" }} />
//                                 <h6 className="col-6 pt-3">{this.props.post_data.user_name}</h6>
//                                 <i className="col-1 align-items-end fas fa-ellipsis-h pt-4"></i>

//                                 <img className="card-img-top  mt-2" src={this.props.post_data.post_url} alt="Card img cap" />

//                                 <div className="col-12  d-flex mt-2 justify-content-between px-0">
//                                     <div className="col-7 d-flex justify-content-around pl-0">

//                                         <Like addLike ={() => this.props.addLike(this.props.post_data.post_id)} removeLike={() => this.props.removeLike(this.props.post_data.post_id)} 
//                                         liked={!this.props.post_data.liked} key={this.props.post_data.post_id}/>

//                                         <i className=" col-2 far fa-comment fa-2x fa-flip-horizontal px-0 text-secondary"></i>
//                                         <i className="col-2 far fa-paper-plane fa-2x px-0 text-secondary"></i>
//                                     </div>

//                                     {/* <Bookmark addBookmark ={() => this.props.addBookmark(this.props.post_data.post_id)} removeBookmark={() => this.props.removeBookmark(this.props.post_data.post_id)} 
//                                          bookmarked={!this.props.post_data.bookmarked} key={this.props.post_data.post_id} /> */}
//                                 </div>
//                                 <div className="col-md-12">
//                                 <div className="col-md-2 pl-0">{this.props.post_data.likes} Likes </div>
//                                 </div>


//                                 <p className="mb-0 col-12">{this.props.post_data.caption}</p>

//                                 <div className="text-secondary align-self-start">x DAY AGO</div>
//                             </div>
//                         </div>
//         );
//     }
// }

// export default PostCard;