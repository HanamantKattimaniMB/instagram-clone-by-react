import React, { Component } from 'react'
import { connect } from "react-redux"
import Like from "./Like.js"
import {
    onChangeProfileImage,
    onChangeCurrentEmail,
    onChangeSelectedPost,
    onChangeLikeColor,
    onChangeBookmarkColor,
    onChangeGetArrayOfLikedPostId,
    onChangeArrayOfLikedPost,
} from "../Redux/action" 

import Bookmark from './Bookmark.js'

const axios = require('axios')

class PostContainer extends Component {

    addLike = (post_id) => {
        console.log("added like")
        axios.post("https://instagram-clone-backend-hk.herokuapp.com/api/add_like", {
            "userId": post_id,
            "postId": localStorage.getItem("user_id")
        })
            .then((data) => {
                console.log(data)
                this.getPostData()
            })
            .catch(error => { 
                console.log(error) 
            })
    }

    removeLike = (post_id) => {
        console.log("removed like")
        axios.post("https://instagram-clone-backend-hk.herokuapp.com/api/remove_like", {
            "userId":localStorage.getItem("user_id"),
            "postId":  post_id
        })
            .then((data) => { 
                console.log(data)   
                this.getPostData()
             })
            .catch(error => { console.log(error) })
    }

    addBookmark = (post_id) => {
        console.log("add bookmark...........")
        axios.post("https://instagram-clone-backend-hk.herokuapp.com/api/add_bookmark", {
            "userId": localStorage.getItem("user_id"),
            "postId": post_id
        })
        .then((data) => {
            console.log(data)
        })
        .catch(error => { console.log(error) })
    }

    removeBookmark = (post_id) => {
        console.log("remove bookmark...........")
        axios.post("https://instagram-clone-backend-hk.herokuapp.com/api/remove_bookmark", {
            "userId":localStorage.getItem("user_id"),
            "postId":  post_id
        })
        .then(data => { console.log(data) })
        .catch(error => { console.log(error) })
    }

    toggleBookmark = (post_data) => {
        const newArr = this.props.arrayPost.map((post)=>{
            if(post.post_id === post_data.post_data){
                post_data.liked = !post_data.liked
                return post_data
            }else{
                return post
            }
        })

    }

    toggleLikeAndUnlike = (post_data) => {
        const newArr = this.props.arrayPost.map((post)=>{
            if(post.post_id === post_data.post_data){
                post_data.liked = !post_data.liked
                return post_data
            }else{
                return post
            }
        })
        this.props.onChangeArrayOfLikedPost(newArr)
    }

  
    getPostData = async () => {

        const likes = await axios.post(`https://instagram-clone-backend-hk.herokuapp.com/api/likesOfCurrentUser`, {
            "userID": localStorage.getItem("user_id")
        })

        const posts = await   axios.get(`https://instagram-clone-backend-hk.herokuapp.com/api/post`, {
            "email": localStorage.getItem("user")
        })

        const Bookmarks = await axios.post(`https://instagram-clone-backend-hk.herokuapp.com/api/bookmarkOfCurrentUser`, {
            "userID": localStorage.getItem("user_id")
        })
        
        const arr = posts.data.map((post)=>{

            const liked = likes.data.find((x)=>{

                return  x === post.post_id
                 
            })
            if(liked) {
                post.liked = true
            }   else    {
                post.liked = false
            }


            const bookmarked = Bookmarks.data.find((x)=>{
                return  x === post.post_id
                 
            })

            if(bookmarked) {
                post.bookmarked = true
                return post
            }   else    {
                post.bookmarked = false
                return post
            }

        })
        this.props.onChangeArrayOfLikedPost(arr)
    }

    componentDidMount() {
        this.getPostData()
    }

    render() {
        let arrayOfPost = this.props.arrayPost
        const reversedPosts = arrayOfPost.map(post => post).reverse();
        return (
            
            <div className="container col-md-6 col-12 row align-self-start ml-md-5 mx-0 px-0 mt-md-5 mt-2">
                {reversedPosts.map(post_data => {
                    return (
                        <div className="card col-12 pt-5 align-items-center pr-3  ml-md-5" style={{ width: "40rem" }}>
                            <div className="card-body col-12 row justify-content-between">
                                <img className="col-3 col-md-2  rounded-circle " src={post_data.profile_picture} alt="Img not loaded" style={{ height: "50px", width: "50px" }} />
                                <h6 className="col-6 pt-3">{post_data.user_name}</h6>
                                <i className="col-1 align-items-end fas fa-ellipsis-h pt-4"></i>

                                <img className="card-img-top  mt-2" src={post_data.post_url} alt="Card img cap" />

                                <div className="col-12  d-flex mt-2 justify-content-between px-0">
                                    <div className="col-7 d-flex justify-content-around pl-0">

                                        <Like 
                                            addLike ={() => this.addLike(post_data.post_id)} 
                                            removeLike={() => this.removeLike(post_data.post_id)} 
                                            post={post_data} key={post_data.post_id} 
                                        />

                                        <i className=" col-2 far fa-comment fa-2x fa-flip-horizontal px-0 text-secondary"></i>
                                        <i className="col-2 far fa-paper-plane fa-2x px-0 text-secondary"></i>
                                    </div>
                                    <Bookmark addBookmark ={() => this.addBookmark(post_data.post_id)} removeBookmark={() => this.removeBookmark(post_data.post_id)} 
                                         bookmarked={!post_data.bookmarked} key={post_data.post_id} />
                                </div>
                                <div className="col-md-12">
                                <div className="col-md-2 pl-0">{post_data.likes} Likes </div>
                                </div>


                                <p className="mb-0 col-12">{post_data.caption}</p>

                                <div className="text-secondary align-self-start">x DAY AGO</div>
                            </div>
                        </div>)
                })}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        profileImage: state.profileImage,
        currentUserEmail: state.currentUserEmail,
        selectedPost: state.selectedPost,
        like_color: state.like_color,
        isBookMarkActive: state.isBookMarkActive,
        arrayOfLikedPostId: state.arrayOfLikedPostId,
        arrayPost:state.arrayPost
    }
}

const mapDispatchToProps = {
    onChangeProfileImage,
    onChangeCurrentEmail,
    onChangeSelectedPost,
    onChangeLikeColor,
    onChangeBookmarkColor,
    onChangeGetArrayOfLikedPostId,
    onChangeArrayOfLikedPost,
}

export default connect(mapStateToProps, mapDispatchToProps)(PostContainer)