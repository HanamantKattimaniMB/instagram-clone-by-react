import React, { Component } from 'react';
import validator from '../Methods/joiValidation';
import {Link} from "react-router-dom"
import * as ReactBootstrap from 'react-bootstrap'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {connect} from "react-redux";
import { 
        onChangeSpinner,
        onChangeEnableButton,
        onChangeProfilePicture,
        setInputData,
       } from "../Redux/action"

const axios = require('axios')

toast.configure()
class Signup extends Component {

    submitted = () => toast("Please wait for a moment!",{
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    });

    signupValidation = async (event) => {
        const {name, value} = event.target
        const result = await validator({[name]: value})
     
        this.props.setInputData({[name]: value})

        if (result !== "success") {
            this.props.setInputData( {
                error: {
                    ...this.props.error,
                    [name]: result
                }
            })
        } else {
            this.props.setInputData({
                error: {
                    [name]: ""
                }
            })
        }
    }

    submitSignupData =  () => {
        this.props.onChangeSpinner("true")
        this.props.onChangeEnableButton("true")
        this.submitted()

        axios.post("https://instagram-clone-backend-hk.herokuapp.com/api/submit_signup_data",{     

                        email:this.props.email,
                        password:this.props.password,
                        username:this.props.userName,
                        fullname:this.props.fullName, 
                        profile_picture:this.props.profile_picture

                    }).then((response)=> {

                        console.log(response.data)
                        if(this.props.userName !== "" && this.props.email !== "")  {
                            this.props.onChangeSpinner("false")
                            this.props.onChangeEnableButton("false")
                            this.props.history.push("/login")
                        }

                    }).catch((error)=>{
                        console.log(error)
                    })
    }

    render() {
        return (
            <div className="col-md-12 border d-flex row justify-content-center" style={{backgroundColor:"#fafafa", height:"50rem"}}>
                        <ToastContainer
                            position="top-right"
                            autoClose={5000}
                            hideProgressBar={false}
                            newestOnTop={false}
                            closeOnClick
                            rtl={false}
                            pauseOnFocusLoss
                            draggable
                            pauseOnHover
                        />
                
                <div className="mt-5 border" style={{height:"550px",width:"350px", backgroundColor:"#ffffff"}}>
               
                    <h2 className="mt-5">Instagram</h2>
                    <p className="text-secondary col-md-8 col-8 ml-5 mt-2">Sign up to see photos and videos from your friends.</p>
             
                    <div className="col-10 ml-4" style={{height:"25rem"}}>
                        <input disabled={ this.props.enableButton ==="false" ? false:true} className="form-control mt-2" name = "email" type="text" onChange={this.signupValidation} placeholder="Email ID"></input>
                        <div className="text-danger">{this.props.error.email}</div>

                        <input disabled={ this.props.enableButton ==="false" ? false:true} className="form-control mt-2" name = "fullName" type="text" onChange={this.signupValidation} placeholder="Full name"></input>
                        <div className="text-danger">{this.props.error.fullName}</div>

                        <input disabled={ this.props.enableButton ==="false" ? false:true} className="form-control mt-2" name = "userName" type="text" onChange={this.signupValidation} placeholder="Username"></input>
                        <div className="text-danger">{this.props.error.userName}</div>
                        
                        <input disabled={ this.props.enableButton ==="false" ? false:true} className="form-control mt-2" name = "password" type="password" onChange={this.signupValidation} placeholder="Password"></input>
                        <div className="text-danger">{this.props.error.password}</div>

                        <button disabled={ this.props.email && this.props.password  !=="" 
                                && (this.props.error.password ==="" || this.props.error.password === undefined)
                                && (this.props.error.email ==="" || this.props.error.email ===undefined) 
                                && (this.props.error.fullname ==="" || this.props.error.fullName ===undefined) 
                                && (this.props.error.userName ==="" || this.props.error.userName ===undefined) 
                                ? false:true} className="col-md-12 btn btn-primary btn-lg mt-2" type="button" onClick={this.submitSignupData}>

                                    {this.props.spinner === "true" ? <ReactBootstrap.Spinner animation="border" variant="white"/>:""} 
                                    {this.props.enableButton ==="false" ? <div> Sign Up</div>:<div></div>}  

                        </button>
                        <div className="mt-3">Already have account? <Link to = "/login" >Login</Link></div>
                    </div>
                    <div className="col-md-11 col-11 text-secondary">By signing up, you agree to our Terms , Data Policy and Cookies Policy .</div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        spinnerOnSignupButton: state.spinnerOnSignupButton,
        spinner: state.spinner,
        enableButton: state.enableButton,
        profile_picture: state.profile_picture,
        email: state.email,
        fullName: state.fullName,
        userName: state.userName,
        password: state.password,
        error: state.error,
    }
}

const mapDispatchToProps = {
    onChangeSpinner,
    onChangeEnableButton,
    onChangeProfilePicture,
    setInputData,
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup)