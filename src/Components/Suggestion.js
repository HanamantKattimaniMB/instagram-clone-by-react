import React, { Component } from 'react';
import profileImage from "../Images/ProfileImages/profileImage.png"
class Suggestion extends Component {
    render() {
        return (
            <div className="col-4 mt-4 d-none d-md-block" style={{position:"absolute",left:"50rem", top:"3rem", backgroundColor:"#fafafa"}}>
                <div className="col-12 mt-2 row align-items-center">
                    <img className="col-3 " src={localStorage.getItem("profile_picture")} alt="Img not loaded"/>
                    <div className="col-7">
        <div className="col-12 ">{localStorage.getItem("user_id")}</div>
        <div className="col-12 text-secondary">{localStorage.getItem("full_name")}</div>
                    </div>
                    <a className="mt-3" href="example.com">Switch</a>
                </div>

                <div className="col-12 row justify-content-between">
                    <h6 className="col-6 mt-2">Suggestions For You</h6>
                    <a href="example.com"> See All</a>
                </div>

                <div className="suggestions flex-column justify-content-center">
                    <div className="col-12 mt-2 row align-items-center justify-content-between">
                    <img className="col-2 " src={profileImage} alt="Img not loaded"/>
                    <div className="col-7">
                        <div className="col-12 ">lakan123</div>
                        <div className="col-12 text-secondary ">Suggested for you</div>
                    </div>
                    <a className="mt-3" href="example.com">Follow</a>
                    </div>

                    <div className="col-12 mt-2 row align-items-center justify-content-between">
                    <img className="col-2 " src={profileImage} alt="Img not loaded"/>
                    <div className="col-7">
                        <div className="col-12 ">suraj_patil5</div>
                        <div className="col-12 text-secondary">Suggested for you</div>
                    </div>
                    <a className="mt-3" href="example.com">Follow</a>
                    </div>

                    <div className="col-12 mt-2 row align-items-center justify-content-between">
                    <img className="col-2 " src={profileImage} alt="Img not loaded"/>
                    <div className="col-7">
                        <div className="col-12 ">Raj</div>
                        <div className="col-12 text-secondary ">Suggested for you</div>
                    </div>
                    <a className="mt-3" href="example.com">Follow</a>
                    </div>

                    <div className="col-12 mt-2 row align-items-center justify-content-between">
                    <img className="col-2 " src={profileImage} alt="Img not loaded"/>
                    <div className="col-7">
                        <div className="col-12 ">Shaym</div>
                        <div className="col-12 text-secondary ">FSuggested for you</div>
                    </div>
                    <a className="mt-3" href="example.com">Follow</a>
                    </div>

                    <div className="col-12 mt-2 row align-items-center justify-content-between">
                    <img className="col-2 " src={profileImage} alt="Img not loaded"/>
                    <div className="col-7">
                        <div className="col-12 ">Karan</div>
                        <div className="col-12 text-secondary ">Suggested for you</div>
                    </div>
                    <a className="mt-3" href="example.com">Follow</a>
                    </div>

                    <div className="col-12 mt-2 row align-items-center justify-content-between">
                    <img className="col-2 " src={profileImage} alt="Img not loaded"/>
                    <div className="col-7">
                        <div className="col-12 ">UserID</div>
                        <div className="col-12 text-secondary ">Suggested for you</div>
                    </div>
                    <a className="mt-3" href="example.com">Follow</a>
                    </div>
                </div>

                <div className="col-12 text-secondary">
                    About Help PressA PI Jobs Privacy Terms Locations Top Accounts Hashtags Language English
                    © 2021 INSTAGRAM FROM FACEBOOK
                </div>
            </div>
        );
    }
}

export default Suggestion;