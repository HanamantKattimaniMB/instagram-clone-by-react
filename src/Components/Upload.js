import React, { Component } from 'react';
import axios from "axios";
import * as ReactBootstrap from 'react-bootstrap'

import {connect} from "react-redux";

import { 
        onChangeLoadingState,
        // onChangeImageFile,
        // onChangeChangeImagePreview,
       } from "../Redux/action"

class Upload extends Component {

    custom_file_upload_url = `https://instagram-clone-backend-hk.herokuapp.com/api/profile`;

    constructor(props) {
        super(props);
        this.state = {
            // loading_state:false,
            image_file: null,
            image_preview: "https://i.pinimg.com/736x/94/88/e4/9488e441128ab3ec547d2f85478e3f08.jpg",
        }
    }

    handleImagePreview = (e) => {
        let image_as_base64 = URL.createObjectURL(e.target.files[0])
        let image_as_files = e.target.files[0];

        this.setState({
            image_preview: image_as_base64,
            image_file: image_as_files,
        })
    }

    handleSubmitFile = () => {
        this.props.onChangeLoadingState(true)
        if (this.state.image_file !== null){

            let formData = new FormData();
            formData.append('image', this.state.image_file);
            formData.append("user_id",2)
            formData.append('caption',"nice pice")

            axios.post(
                this.custom_file_upload_url,
                formData,
                {
                    headers: {
                        "Authorization": "YOUR_API_AUTHORIZATION_KEY_SHOULD_GOES_HERE_IF_HAVE",
                        "Content-type": "multipart/form-data",
                    },                    
                }
            )
            .then(res => {
                console.log(`Success` + res.data);
                this.props.onChangeLoadingState(false)
                this.props.history.push("/home")
            })
            .catch(err => {
                console.log(err);
            })
        }
    }

    render() { 
        return (
            <div className="row px-md-5 py-0" style={{height:"800px", backgroundColor:"#f5d3cd", backgroundImage: `url(require("../Images/background/instagram-background.jpg"))`}}>
               
               <div className="border col-md-6 border-none mt-2 "> 
                    <img className="col-md-12 mt-md-3 mt-0 px-0" src={this.state.image_preview} alt="" style={{height:"450px" , width:"300px"}}/>
               </div>
               <div className="border col-md-6 "> 
                    <input  className="col-12 mt-md-5 mt-1" 
                                    type="file"
                                    onChange={this.handleImagePreview}
                                    style={{backgroundColor:"#fce4e0"}}
                                />
                    <div class="md-form mt-md-5 mt-1">
                    <textarea id="form7" class="md-textarea form-control" placeholder="Caption" rows="3"></textarea>
                    </div>
                    {this.props.loading_state === true ? <ReactBootstrap.Spinner animation="border" variant="white"/>:""} 
                    <input className="col-12 border-none mt-md-5 mt-1" style={{backgroundColor:"#fce4e0"}} type="submit" onClick={this.handleSubmitFile} value="Post"/> 
                            
               </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loading_state: state.loading_state,
        // image_file: state.image_file,
        // image_preview: state.image_preview
    }
}

const mapDispatchToProps = {
    onChangeLoadingState,
    // onChangeImageFile,
    // onChangeChangeImagePreview,
}
export default connect(mapStateToProps, mapDispatchToProps)(Upload)












