import Joi from 'joi'

const validator = (obj)=>{
    return new Promise((resolve,reject)=>{
        const schema = Joi.object({
            fullName: Joi.string().min(4),
            userName: Joi.string().min(4),
            email: Joi.string().email({
                 minDomainSegments: 2, tlds: { allow: ['com', 'net',"tech","io"] }
            }),
            password: Joi.string().min(5),
        });
        const result = schema.validate(obj);
        if(result.error){
            return resolve(result.error.details[0].message)
        }else{
            return resolve("success")
        }
    })
}

export default validator
