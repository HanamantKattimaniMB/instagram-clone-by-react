const onChangeProfileImage = (data) => {
    return {
        type : "CHANGE_PROFILE_IMAGE",
        payload : {data}
    }
}

const onChangeCurrentEmail = (data) => {
    return {
        type : "CHANGE_CURRENT_USER_EMAIL",
        payload : {data}
    }
}

const onChangeSelectedPost = (data) => {
    return {
        type : "CHANGE_SELECTED_POST",
        payload : {data}
    }
}

const onChangeLikeColor = (data) => {
    return {
        type : "CHANGE_LIKE_COLOR",
        payload : {data}
    }
}

const onChangeBookmarkColor = (data) => {
    return {
        type : "CHANGE_BOOKMARK_COLOR",
        payload : {data}
    }
}


//======LOGIN=======
const onChangeEmail = (data) => {
    return {
        type : "CHANGE_EMAIL",
        payload : {data}
    }
}

const onChangePassword = (data) => {
    return {
        type : "CHANGE_PASSWORD",
        payload : {data}
    }
}

const onChangeIsEnable = (data) => {
    return {
        type : "CHANGE_IS_ENABLE",
        payload : {data}
    }
}


const onChangeSpinner = (data) => {
    return {
        type : "CHANGE_SPINNER",
        payload : {data}
    }
}

const onChangeEnableButton = (data) => {
    return {
        type : "CHANGE_ENABLE_BUTTON",
        payload : {data}
    }
}

const onChangeDisableIfEmpty = (data) => {
    return {
        type : "CHANGE_DISABLE_IF_EMPTY",
        payload : {data}
    }
}

const onChangeIsValidUser = (data) => {
    return {
        type : "CHANGE_IS_VALID_USER",
        payload : {data}
    }
}

const onChangeLoadingState = (data) => {
    return {
        type : "CHANGE_LOADING_STATE",
        payload : {data}
    }
}


const onChangeImageFile = (data) => {
    return {
        type : "CHANGE_IMAGE_FILE",
        payload : {data}
    }
}


const onChangeChangeImagePreview = (data) => {
    return {
        type : "CHANGE_IMAGE_PREVIEW",
        payload : {data}
    }
}


const onChangeSpinnerOnSignupButton = (data) => {
    return {
        type : "CHANGE_SPINNER_ON_SIGNUP_BUTTON",
        payload : {data}
    }
}

const onChangeProfilePicture = (data) => {
    return {
        type : "CHANGE_PROFILE_PICTURE",
        payload : {data}
    }
}


const setInputData = (data) => {
    return {
        type : "CHANGE_INPUT_DATA",
        payload : data
    }
}

const onChangeGetArrayOfLikedPostId = (data) => {
    return {
        type : "CHANGE_GET_LIKED_POST",
        payload : {data}
    }
}

const onChangeArrayOfLikedPost = (data) => {
    return {
        type : "CHANGE_ARRAY_OF_LIKED_POST",
        payload : {data}
    }
}

const onLogin = (data) => {
    return {
        type : "CHANGE_ON_LOGIN",
        payload : {data}
    }
}

const onChangeLikeState = (data) => {
    return {
        type : "ON_CHANGE_LIKE_STATE",
        payload : {data}
    }
}

const onChangeBookmark = (data) => {
    console.log("method called ", data)
    return {
        type : "CHANGE_ON_CLICK_BOOKMARK",
        payload : {data}
    }
}


module.exports = {
    onChangeProfileImage,
    onChangeCurrentEmail,
    onChangeSelectedPost,
    onChangeLikeColor,
    onChangeBookmarkColor,
    onChangeEmail,
    onChangePassword,
    onChangeIsEnable,
    onChangeSpinner,
    onChangeEnableButton,
    onChangeDisableIfEmpty,
    onChangeIsValidUser,
    onChangeLoadingState,
    onChangeImageFile,
    onChangeChangeImagePreview,
    onChangeSpinnerOnSignupButton,
    onChangeProfilePicture,
    setInputData,
    onChangeGetArrayOfLikedPostId,
    onChangeArrayOfLikedPost,
    onLogin,
    onChangeLikeState,
    onChangeBookmark,
}