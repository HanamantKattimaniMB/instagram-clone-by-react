const initialState = {
    profileImage:"",
    currentUserEmail:"lakan1998hpk@gmail.com",
    selectedPost:[],
    like_color:"true",
    isBookMarkActive:"true",
    arrayOfLikedPostId:[1,2,3,51,81,4],
    arrayPost:[],
    likeState:null,
    arrayOfBookMarkedPosts:[1,2,3],

    email: "",            
    password: "",
    isEnable: "disable",
    isValidUser: "true",
    spinner:"false",
    enableButton:"false", 
    disableIfEmpty:"true", 
    onLoginState:false,
    
    error: {
        email: '',
        password: "",
    },

    loading_state:false,
    image_file: null,
    image_preview: "",
    onChangeSpinnerOnSignupButton:"false",
    profile_picture:"https://www.pngfind.com/pngs/m/381-3819326_default-avatar-svg-png-icon-free-download-avatar.png",
}

const reducer = (state = initialState, action) => {
    switch(action.type){

        case "ON_CHANGE_LIKE_STATE" :
            return {
                ...state,
                profileImage : action.payload.data
            }

        case "CHANGE_PROFILE_IMAGE" : 
        return {
            ...state,
            profileImage : action.payload.data
        }

        case "CHANGE_CURRENT_USER_EMAIL" :
        return {
            ...state,
            currentUserEmail : action.payload.data
        }

        case "CHANGE_SELECTED_POST" :
            return {
                ...state,
                selectedPost : action.payload.data
            }

        case "CHANGE_LIKE_COLOR" :
            return {
                ...state,
                like_color : action.payload.data
            }

        case "CHANGE_BOOKMARK_COLOR" :
            return {
                ...state,
                isBookMarkActive : action.payload.data
            }

        case "CHANGE_EMAIL" :
            return {
                ...state,
                email : action.payload.data
            }    
            
        case "CHANGE_PASSWORD" :
            return {
                ...state,
                password : action.payload.data
            }

        case "CHANGE_IS_ENABLE" :
            return {
                ...state,
                isEnable : action.payload.data
            }

        case "CHANGE_SPINNER" :
            return {
                ...state,
                spinner : action.payload.data
            }

        case "CHANGE_ENABLE_BUTTON" :
            return {
                ...state,
                enableButton : action.payload.data
            }

        case "CHANGE_DISABLE_IF_EMPTY" :
            return {
                ...state,
                disableIfEmpty : action.payload.data
            }
        
        case "CHANGE_IS_VALID_USER" :
        return {
            ...state,
            isValidUser : action.payload.data
        }

        case "CHANGE_LOADING_STATE" :
            return {
                ...state,
                loading_state : action.payload.data
            }

        case "CHANGE_IMAGE_FILE" :
            return {
                ...state,
                image_file : action.payload.data
            }

        case "CHANGE_IMAGE_PREVIEW" :
            return {
                ...state,
                image_preview : action.payload.data
            }

        case "CHANGE_SPINNER_ON_SIGNUP_BUTTON" :
            return {
                ...state,
                image_preview : action.payload.data
            }
        
        case "CHANGE_PROFILE_PICTURE" :
            return {
                ...state,
                profile_picture : action.payload.data
            }

        case "CHANGE_INPUT_DATA" :
            const   payload  = action.payload
            return {
                ...state,
                ...payload
            }

        case "CHANGE_GET_LIKED_POST" :
            return {
                ...state,
                arrayOfLikedPostId : action.payload.data
            }

        case "CHANGE_ARRAY_OF_LIKED_POST" :
            return {
                ...state,
                arrayPost : action.payload.data
            }

        case "CHANGE_ON_LOGIN":
            return  {
                ...state,
                onLoginState : action.payload.data
            }
            
        case "CHANGE_ON_CLICK_BOOKMARK":
            console.log(action.payload.data,"dklkkkkkkkkkkkkkkkkkkkkk")
            return  {
                ...state,
                arrayOfBookMarkedPosts : action.payload.data
            }
            
        default : return state
    }
}

export default reducer